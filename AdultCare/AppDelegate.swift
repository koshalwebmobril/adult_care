//
//  AppDelegate.swift
//  AdultCare
//
//  Created by Manish on 31/05/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
            if UserDefaults.standard.getToken() != ""{
                gotoHomeVC()
            }else{
                gotoLoginVC()
            }
             
            
        
        return true
    }
    
func gotoHomeVC(){
    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    let initialViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
   
    let navigationController = UINavigationController(rootViewController: initialViewController)
    navigationController.isNavigationBarHidden = true
    self.window = UIWindow(frame: UIScreen.main.bounds)
    self.window?.rootViewController = navigationController
    self.window?.makeKeyAndVisible()
}

func gotoLoginVC(){
    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    let initialViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
   
    let navigationController = UINavigationController(rootViewController: initialViewController)
    navigationController.isNavigationBarHidden = true
    self.window = UIWindow(frame: UIScreen.main.bounds)
    self.window?.rootViewController = navigationController
    self.window?.makeKeyAndVisible()
}

    
    


}

