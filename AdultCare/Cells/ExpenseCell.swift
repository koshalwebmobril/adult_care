//
//  ExpenseCell.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 17/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import iOSDropDown

class ExpenseCell: UITableViewCell {
    
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblSubcategory: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    //HeaderCell
    
    @IBOutlet weak var txtPayee: DropDown!
    
    @IBOutlet weak var txtPaymentAmount: DropDown!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtPaymentMethod: DropDown!
    @IBOutlet weak var txtRef: UITextField!
    @IBOutlet weak var lblAllAmount: UILabel!
    
    
    //SectionCell
    
    @IBOutlet weak var txtSelectCategory: DropDown!
    @IBOutlet weak var txtSelectSubcategory: DropDown!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    
    
    //FooterCell
    
    @IBOutlet weak var btnAddNew: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var txtAttach: UITextField!
    @IBOutlet weak var txtMemo: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnPrint: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
