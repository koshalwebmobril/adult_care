//
//  DataManager.swift
//  Test
//
//  Created by Koshal Saini on 13/05/20.
//  Copyright © 2020 Koshal Saini. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON


class DataManager: NSObject {
    // MARK: -
    // MARK: - Common Service Function with JSON Response
    //*****!!-------------------------!!***** !!-------!! *****!!-------------------------!!*****//
    //*****!!-------------------------!!***** For Get Api *****!!-------------------------!!*****//
    /**
     @discussion:    function to call the service for JSON response
     @paramters:     (url: String, viewcontroller : UIViewController!,parameters: [String: AnyObject], completionHandler: @escaping     (AnyObject?, NSError?) -> ())
     @return: Nil
     */
    class func alamofireGetRequest(urlString:String, viewcontroller : UIViewController!,completion: @escaping (_ data: JSON?, _ error: NSError?)->Void){
        print(urlString)
        MBProgressHUD.showHUDMessage(message: "", PPview: viewcontroller.view)
        if  Utility.isInternetAvailable() == true {
            AF.request(urlString, method: .get, parameters: [Constant().BLANK:Constant().BLANK], encoding: URLEncoding.default, headers: nil).responseJSON { response in
                MBProgressHUD.hideHUD()
                switch(response.result) {
                case .success(let value):
                    if let data = value as? [String:Any]{
                        
                        let json = JSON(data)
                        completion(json, nil)
                    }
                case .failure(let error):
                    print(error)
                    Utility.showAlertMessage(title: Constant().BLANK, message: (error.localizedDescription), view: viewcontroller)
                }
                MBProgressHUD.hideHUD()
            }
        }else{
            Utility.showAlertMessage(title: Constant().BLANK, message: Constant().MESSAGE, view: viewcontroller)
            MBProgressHUD.hideHUD()
        }
    }
    
    
    class func alamofirePostRequest(urlString:String, param:[String:Any],viewcontroller : UIViewController!,completion: @escaping (_ data: JSON?, _ error: NSError?)->Void){
        print(urlString)
        MBProgressHUD.showHUDMessage(message: "", PPview: viewcontroller.view)
        if  Utility.isInternetAvailable() == true {
            
            AF.request(urlString, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                MBProgressHUD.hideHUD()
                switch(response.result) {
                case .success(let value):
                    if let data = value as? [String:Any]{
                        
                        let json = JSON(data)
                        completion(json, nil)
                    }
                case .failure(let error):
                    print(error)
                    Utility.showAlertMessage(title: Constant().BLANK, message: (error.localizedDescription), view: viewcontroller)
                }
                MBProgressHUD.hideHUD()
            }
        }else{
            Utility.showAlertMessage(title: Constant().BLANK, message: Constant().MESSAGE, view: viewcontroller)
            MBProgressHUD.hideHUD()
        }
    }
    
   
    
    class func alamofirePostHeaderRequest(urlString:String, param:[String:Any],viewcontroller : UIViewController!,completion: @escaping (_ data: JSON?, _ error: NSError?)->Void){
        print(urlString)
        MBProgressHUD.showHUDMessage(message: "", PPview: viewcontroller.view)
        if  Utility.isInternetAvailable() == true {
            
            let token = UserDefaults.standard.getToken()
               
               let header: HTTPHeaders = ["Authorization": "Bearer "+token]
            
            AF.request(urlString, method: .post, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON { response in
                MBProgressHUD.hideHUD()
                switch(response.result) {
                case .success(let value):
                    if let data = value as? [String:Any]{
                        
                        let json = JSON(data)
                        completion(json, nil)
                    }
                case .failure(let error):
                    print(error)
                    Utility.showAlertMessage(title: Constant().BLANK, message: (error.localizedDescription), view: viewcontroller)
                }
                MBProgressHUD.hideHUD()
            }
        }else{
            Utility.showAlertMessage(title: Constant().BLANK, message: Constant().MESSAGE, view: viewcontroller)
            MBProgressHUD.hideHUD()
        }
    }
    
    
    class func alamofireGetHeaderRequest(urlString:String, param:[String:Any],viewcontroller : UIViewController!,completion: @escaping (_ data: JSON?, _ error: NSError?)->Void){
        print(urlString)
        MBProgressHUD.showHUDMessage(message: "", PPview: viewcontroller.view)
        if  Utility.isInternetAvailable() == true {
            
            let token = UserDefaults.standard.getToken()
               
               let header: HTTPHeaders = ["Authorization": "Bearer "+token]
            
            AF.request(urlString, method: .get, parameters: param, encoding: URLEncoding.default, headers: header).responseJSON { response in
                MBProgressHUD.hideHUD()
                switch(response.result) {
                case .success(let value):
                    if let data = value as? [String:Any]{
                        
                        let json = JSON(data)
                        completion(json, nil)
                    }
                case .failure(let error):
                    print(error)
                    Utility.showAlertMessage(title: Constant().BLANK, message: (error.localizedDescription), view: viewcontroller)
                }
                MBProgressHUD.hideHUD()
            }
        }else{
            Utility.showAlertMessage(title: Constant().BLANK, message: Constant().MESSAGE, view: viewcontroller)
            MBProgressHUD.hideHUD()
        }
    }
}
