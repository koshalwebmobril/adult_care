//
//  Constant.swift
//  Test
//
//  Created by Koshal Saini on 13/05/20.
//  Copyright © 2020 Koshal Saini. All rights reserved.
//
import Foundation
import UIKit

let BASE_URL = "https://www.webmobril.org/dev/adultcare/api/v1/"
let login_url = "auth/login"
let logout_url = "auth/logout"
let getprofile_url = "profile"
let contact_url = "contact-us"
let privacyPolicy_url = "shared/privacy-policy"
let menulist_url = "menus-list"
let submenulit_url = "submenus-list"

class Constant {
    //***** ------------------ *****//
    //***** for prograss title *****//
    let PROGRESS_TITLE = "Loading..."
    let BLANK = ""
    let MESSAGE = "Please_Check_network_connection"
    
    let TYPE = "type"
    let TITLE = "title"
    let URL = "url"
    let COVER_ASSET = "coverAsset"
    let ICON_ASSET = "iconAsset"
    let CHANNEL = "channel"
    let DATA = "data"
    let MEDIA_COUNT = "mediaCount"
    let ID = "id"
    let THUMBNAIL_URL = "thumbnailUrl"
    let LATEST_MEDIA = "latestMedia"
    let CATEGORIES = "categories"
    let NAME = "name"
    let COURSE = "course"
    let CHANNELS = "channels"
    let MEDIA = "media"
    let OK = "OK"
    let CANCEL = "Cancel"
    let PLACEHOLDER = "placeholder"
    
    
}
