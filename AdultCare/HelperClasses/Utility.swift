//
//  Utility.swift
//  Test
//
//  Created by Koshal Saini on 13/05/20.
//  Copyright © 2020 Koshal Saini. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import SDWebImage


class Utility: NSObject {
    

    /**
     Description: This method will Check Internet Connection
     Parameter: Nil
     Returns:Bool value
     */
    //  MARK:-
    //  MARK:-  Check Internet Connection
    class func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    /**
     @description: Method used for show alert message
     @parameters:  (title: String, message: String, view: UIViewController)
     @returns:Nil
     */
    class func showAlertMessage(title: String, message: String, view: UIViewController) {
        let objAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: Constant().OK, style: .cancel) { action -> Void in
            //Do some other stuff
        }
        objAlert.addAction(nextAction)
        view.present(objAlert, animated: true, completion: nil)
    }
    
    /**
     @description: Method used for show alert message with ok action
     @parameters:  (title: String, message: String, view: UIViewController)
     @returns:Nil
     */
    class func showAlertMessageWithOkAction(title: String, message: String, view: UIViewController, completion: @escaping () -> Void) {
        let objAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: Constant().OK, style: .cancel) { action -> Void in
            completion()
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: Constant().CHANNEL, style: .default) { action -> Void in
        }
        objAlert.addAction(nextAction)
        objAlert.addAction(cancelAction)
        view.present(objAlert, animated: true, completion: nil)
    }
    
    /**
     @description: Method used for show alert message
     @parameters:  (title: String, message: String, view: UIViewController)
     @returns:Nil
     */
    class func showKSAlertMessage(title: String, message: String, view: UIViewController, completion: @escaping () -> Void) {
        let objAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: Constant().OK, style: .cancel) { action -> Void in
            //Do some other stuff
            completion()
        }
        objAlert.addAction(nextAction)
        view.present(objAlert, animated: true, completion: nil)
    }
    
    
    
    /**
     @description: Method used for download image from server
     @parameters:  imageUrl,imageView
     @returns:Nil
     */
    class func downloadProfileImage(url:String,imageView:UIImageView) {
       
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: Constant().PLACEHOLDER))
        imageView.clipsToBounds = true
    }
}
