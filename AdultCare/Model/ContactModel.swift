//
//  ContactModel.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 18/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import SwiftyJSON

class ContactModel :NSObject{
    var status : String?
    var message : String?
    
    init(data:[String: JSON]) {
           message = data["message"]?.stringValue
           status = data["status"]?.stringValue
           
       }
}
