//
//  LoginModel.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 18/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginModel :NSObject{
    var token : String?
    var status : String?
   
    init(data:[String: JSON]) {
        token = data["token"]?.stringValue
        status = data["status"]?.stringValue
        
    }
    
}
