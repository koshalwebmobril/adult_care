//
//  ContactViewModel.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 18/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ContactViewModel {
    
   func callContactUSWebServiceMethod(baseUrl:String,param:[String:Any],viewController:UIViewController, completion: @escaping (ContactModel) -> Void) {
          DataManager.alamofirePostHeaderRequest(urlString: baseUrl,param: param, viewcontroller: viewController) { (responseObject, error) in
              let arryValueInfo = responseObject
             
             
              let contactModel : ContactModel = ContactModel.init(data: arryValueInfo?.dictionaryValue ?? [:])
                
              completion(contactModel)
          }
      }
      
    
    
    
}
