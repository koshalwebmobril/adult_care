//
//  LoginViewModel.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 18/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import Alamofire

class LoginViewModel {
    
    
    
    
    func callLoginWebServiceMethod(baseUrl:String,param:[String:Any],viewController:UIViewController, completion: @escaping (LoginModel) -> Void) {
        DataManager.alamofirePostRequest(urlString: baseUrl,param: param, viewcontroller: viewController) { (responseObject, error) in
            let arryValueInfo = responseObject
           
           
            let loginModel : LoginModel = LoginModel.init(data: arryValueInfo?.dictionaryValue ?? [:])
              
            completion(loginModel)
        }
    }
    
    
    
    
}
