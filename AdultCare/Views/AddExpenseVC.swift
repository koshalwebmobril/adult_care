//
//  AddExpenseVC.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 17/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddExpenseVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    

    @IBOutlet weak var tableView: UITableView!
    
    var count = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let superCell = tableView.dequeueReusableCell(withIdentifier: "SectionCell", for: indexPath) as! ExpenseCell
        superCell.txtDescription.layer.masksToBounds = true
        superCell.txtDescription.layer.cornerRadius = 5.0
        superCell.txtDescription.layer.borderWidth = 1.0
        superCell.txtDescription.layer.borderColor = UIColor.baseBlue.cgColor
        return superCell
       }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 475.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! ExpenseCell
        
        
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 450.0
    }
   
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! ExpenseCell
        footerCell.txtMemo.layer.masksToBounds = true
        footerCell.txtMemo.layer.cornerRadius = 5.0
        footerCell.txtMemo.layer.borderWidth = 1.0
        footerCell.txtMemo.layer.borderColor = UIColor.baseBlue.cgColor
        
        footerCell.btnAddNew.underline()
        footerCell.btnClear.underline()
        
        footerCell.btnSave.layer.masksToBounds = true
        footerCell.btnSave.layer.cornerRadius = 5.0
        
        
        footerCell.btnCancel.layer.masksToBounds = true
        footerCell.btnCancel.layer.cornerRadius = 5.0
        
        footerCell.btnPrint.layer.masksToBounds = true
        footerCell.btnPrint.layer.cornerRadius = 5.0
        
        
        return footerCell
    }
    
    @IBAction func addNewAction(_ sender: UIButton) {
        
        count = count + 1
        
        tableView.reloadData()

    }
    
    
    @IBAction func btnClearAction(_ sender: UIButton) {
    }
    
    @IBAction func bellAction(_ sender: Any) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
           self.navigationController?.pushViewController(vc, animated: true)
       }
    
    @IBAction func printAction(_ sender: Any) {
        
        
        let pdfFilePath = self.tableView.exportAsPdfFromTable()
        print(pdfFilePath)
        
    }
    
    
    
    
}
