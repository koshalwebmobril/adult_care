//
//  ContactVC.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 17/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class ContactVC: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var txtMessage: UITextView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var messageView: UIView!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        

        setUI()
    }
    
    func setUI(){
        txtSubject.layer.masksToBounds = true
        txtSubject.layer.cornerRadius = 5.0
        txtSubject.layer.borderColor = UIColor.baseBlue.cgColor
        txtSubject.layer.borderWidth = 1.0
        txtSubject.setLeftView(height: txtSubject.frame.height)
        
        messageView.layer.masksToBounds = true
        messageView.layer.cornerRadius = 5.0
        messageView.layer.borderColor = UIColor.baseBlue.cgColor
        messageView.layer.borderWidth = 1.0
        
        btnSubmit.layer.masksToBounds = true
        btnSubmit.layer.cornerRadius = 5.0
        

    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
    @IBAction func submitAction(_ sender: Any) {
        if isValidation(){
            let paramDict = ["subject":txtSubject.text ?? "","message": txtMessage.text ?? ""]
            ContactViewModel().callContactUSWebServiceMethod(baseUrl: BASE_URL + contact_url, param: paramDict, viewController: self) { (ContactModel) in
                 if ContactModel.status ?? "" == "success"{
                    
                    let alert = UIAlertController(title: AppName, message: ContactModel.message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.gotoHomeVC()
                    }))
                    self.present(alert, animated: true, completion: nil)
                              
                           
                           }else{
                    self.createAlert(title: AppName, message:ContactModel.message ?? "")
                           }
            }
        }
    }
    
    func isValidation() -> Bool {
           if txtSubject.text == "" {
               
               createAlert(title: AppName, message: subject)
           
               return false
           }
           else if txtMessage.text == "" {
               
               createAlert(title: AppName, message: message)
               
                   return false
           }
           
           return true
           
       }
}


