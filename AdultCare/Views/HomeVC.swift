//
//  HomeVC.swift
//  AdultCare
//
//  Created by Manish on 31/05/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SideMenu

class HomeVC: UIViewController {

    @IBOutlet weak var residentsView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var scheduleView: UIView!
    @IBOutlet weak var resourceView: UIView!
    @IBOutlet weak var softwareView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setUI()
    }
    
    func setUI(){
        let residentGesture = UITapGestureRecognizer(target: self, action: #selector(residentClick))
        let infoGesture = UITapGestureRecognizer(target: self, action: #selector(infoClick))
        
        let scheduleGesture = UITapGestureRecognizer(target: self, action: #selector(scheduleClick))
        
        let resourceGesture = UITapGestureRecognizer(target: self, action: #selector(resourceClick))
        let softwareGesture = UITapGestureRecognizer(target: self, action: #selector(softwareClick))
        
        residentsView.addGestureRecognizer(residentGesture)
        infoView.addGestureRecognizer(infoGesture)
        scheduleView.addGestureRecognizer(scheduleGesture)
        resourceView.addGestureRecognizer(resourceGesture)
        softwareView.addGestureRecognizer(softwareGesture)
    }
    
    
    @objc func residentClick(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResidentListsVC") as! ResidentListsVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func infoClick(){
        
    }
    
    @objc func scheduleClick(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WorkScheduleVC") as! WorkScheduleVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func resourceClick(){
        
    }
    
    @objc func softwareClick(){
        
    }

   @IBAction func tapOnHamburger(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sidemenuvc = storyboard.instantiateViewController(withIdentifier: "MenuSlideVC") as! MenuSlideVC
        let menu = SideMenuNavigationController(rootViewController: sidemenuvc)
        menu.statusBarEndAlpha = 0
        menu.menuWidth = UIScreen.main.bounds.width / 1.3
        menu.presentationStyle = .menuDissolveIn
        menu.leftSide = true
        menu.presentationStyle.presentingEndAlpha = 0.65
        menu.presentationStyle.onTopShadowOpacity = 0.5
        menu.presentationStyle.onTopShadowRadius = 5
        menu.presentationStyle.onTopShadowColor = .black
        present(menu, animated: true, completion: nil)
    }
}
