//
//  LoginVC.swift
//  AdultCare
//
//  Created by Manish on 31/05/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginVC: UIViewController {
    @IBOutlet weak var txtMail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var bgViewtextField: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    
    let viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        
    }
    
    func setUI() {
        bgViewtextField.layer.cornerRadius = 15.0
        bgViewtextField.clipsToBounds = true
        
        btnForgot.underline()
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: txtPassword.frame.height))
               
        let button = UIButton(frame: CGRect(x: 0, y: 10, width: 30, height: 30))
               button.setImage(UIImage(named: "hide"), for: .normal)
        button.setImage(UIImage(named: "eye"), for: .selected)
        button.addTarget(self, action: #selector(eyeAction(_ :)), for: .touchUpInside)
               view.addSubview(button)
                      
               txtPassword.rightViewMode = .always
               txtPassword.rightView = view
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @objc func eyeAction(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            txtPassword.isSecureTextEntry = false
            
        }else{
            txtPassword.isSecureTextEntry = true
        }
        
    }

    @IBAction func actionLogin(_ sender: Any) {
        if isValidation(){
        /*email
        password
        device_type
        device_token
        device_ip
        lat
        lng*/
        let paramDict = ["email":txtMail.text ?? "" ,"password":txtPassword.text ?? ""," device_type":"2","device_token":"123","device_ip":"128.24.00","lat":"1.233","lng":"1.23"]
        LoginViewModel().callLoginWebServiceMethod(baseUrl: BASE_URL + login_url, param: paramDict, viewController: self) { (LoginModel) in
          
            if LoginModel.status ?? "" == "success"{
                print(LoginModel.token)
                UserDefaults.standard.setToken(token: LoginModel.token ?? "")
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.gotoHomeVC()
            }else{
                self.createAlert(title: AppName, message: "Login failed.")
            }
        }
        }
        
    }

   
    @IBAction func actionForgot(_ sender: Any) {
       
        
    }
    
    func isValidation() -> Bool {
        if txtMail.text == "" {
            
            createAlert(title: AppName, message: valid_Email)
        
            return false
        }else  if !(txtMail.text?.isValidEmail())!{
            createAlert(title: AppName, message: valid_EmailAddress)
            
                return false
        }
        else if txtPassword.text == "" {
            
            createAlert(title: AppName, message: valid_password)
            
                return false
        }
        
        return true
        
    }
    
    func isValidEmail(email: String)-> Bool{
           let emailRegEx = "[A-Za-z0-9.%+-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
           return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: email)
       }

}

