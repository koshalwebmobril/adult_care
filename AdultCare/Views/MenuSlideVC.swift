//
//  MenuSlideVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class MenuSlideVC: UIViewController {
    
    @IBOutlet weak var tblSideMenu: UITableView!
    
    var sideMenuData = ["HOME" , "MY INFORMATION" ,"EXPENSES", "NOTIFICATIONS" , "CONTACT US" , "PRIVACY & POLICY" , "TERMS & CONDITIONS" , "LOGOUT"  ]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool){
    }
    
}
extension MenuSlideVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTblCell" , for: indexPath) as! SideMenuTblCell
        cell.titleLabel.text = sideMenuData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        
        
        switch index {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ExpenseListVC") as! ExpenseListVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 4:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ContactVC") as! ContactVC
            self.navigationController?.pushViewController(vc, animated: true)
            case 5:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "WebViewVc") as! WebViewVc
            vc.isfrom = "privacy"
            self.navigationController?.pushViewController(vc, animated: true)
            case 6:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "WebViewVc") as! WebViewVc
            vc.isfrom = "terms"
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        case 7:
            
            let alert = UIAlertController(title: "AdultCare", message: "Do you want to logout?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {_ in
                
                UserDefaults.standard.setUserId(userId: "")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        default:
            print("")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
}





class SideMenuTblCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
