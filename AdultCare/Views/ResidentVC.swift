//
//  ResidentVC.swift
//  AdultCare
//
//  Created by Manish on 31/05/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class ResidentVC: UIViewController {
    
    

    @IBOutlet weak var basicView: UIView!
    @IBOutlet weak var medicalView: UIView!
    
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var familyView: UIView!
    @IBOutlet weak var managerView: UIView!
    @IBOutlet weak var reportView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      setUI()
    }
    
    @IBAction func bellAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUI(){
        let basicGesture = UITapGestureRecognizer(target: self, action: #selector(basicClick))
        let familyGesture = UITapGestureRecognizer(target: self, action: #selector(familyGestureClick))
        
        let medicalGesture = UITapGestureRecognizer(target: self, action: #selector(medicalGestureClick))
        
        let dailyGesture = UITapGestureRecognizer(target: self, action: #selector(dailyGestureClick))
        let reportGesture = UITapGestureRecognizer(target: self, action: #selector(reportGestureClick))
        let managerGesture = UITapGestureRecognizer(target: self, action: #selector(managerGestureClick))
        
        basicView.addGestureRecognizer(basicGesture)
        medicalView.addGestureRecognizer(medicalGesture)
        activityView.addGestureRecognizer(dailyGesture)
        familyView.addGestureRecognizer(familyGesture)
        managerView.addGestureRecognizer(managerGesture)
        reportView.addGestureRecognizer(reportGesture)
    }
    
    
    @objc func basicClick(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResidentInformationVC") as! ResidentInformationVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func familyGestureClick(){
        
    }
    
    @objc func medicalGestureClick(){
        
    }
    
    @objc func dailyGestureClick(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DailyLivingVC") as! DailyLivingVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func reportGestureClick(){
        
    }
    
    @objc func managerGestureClick(){
        
    }
    
   

}
