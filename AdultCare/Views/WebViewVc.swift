//
//  WebViewVc.swift
//  AdultCare
//
//  Created by Sandeep Kumar on 18/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import WebKit

class WebViewVc: UIViewController,WKNavigationDelegate,UITextFieldDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    var isfrom = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var url : URL?
        if isfrom == "privacy"{
            titleLabel.text = "PRIVACY & POLICY"
             url = URL(string: "\(GlobalURL.baseURL)api/v1/shared/privacy-policy")
        }else{
            titleLabel.text = "TERMS & CONDITIONS"
            url = URL(string: "\(GlobalURL.baseURL)api/v1/shared/terms-and-conditions")
        }

        
        let requestObj = URLRequest(url: url! as URL)
        webView.navigationDelegate = self
        webView.uiDelegate = self as? WKUIDelegate
        webView.scrollView.bounces = false
        webView.load(requestObj)
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
            MBProgressHUD.hideHUD()
        MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
    MBProgressHUD.hideHUD()
    MBProgressHUD.hide(for: (self.view)!, animated: true)
    }

}
